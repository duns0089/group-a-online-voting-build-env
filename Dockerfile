FROM mcr.microsoft.com/windows/servercore:ltsc2019
ENTRYPOINT ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

# Install chocolatey
RUN powershell -Command Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
RUN choco feature disable -name=exitOnRebootDetected
RUN choco feature disable -name=usePackageExitCodes
RUN choco feature disable -name=useEnhancedExitCodes

# Install the Visual Studio installer
RUN choco install -y chocolatey-visualstudio.extension visualstudio-installer

# Install the visual studio build tools
# RUN choco install -y visualstudio2019buildtools --ignore-dependencies --package-parameters "--wait --add Microsoft.VisualStudio.Workload.VCTools --add Microsoft.VisualStudio.Workload.ManagedDesktopBuildTools --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 --add Microsoft.VisualStudio.Component.Windows10SDK.19041"

RUN choco install -y visualstudio2017buildtools --ignore-dependencies --package-parameters "--wait --add Microsoft.VisualStudio.Workload.VCTools --add Microsoft.VisualStudio.Workload.ManagedDesktopBuildTools --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 --add Microsoft.VisualStudio.Component.Windows10SDK.19041"

# Install Python
RUN choco install -y python

# Install git
RUN choco install -y git

# Set Path
RUN setx PATH "%PATH%;C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\MSBuild\Current\Bin;C:\python310;c:\ProgramData\chocolatey\bin;c:\Program Files (x86)\Git\bin;C:\Program Files\Git\bin"
