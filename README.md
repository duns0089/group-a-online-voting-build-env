## Version

Version current set to latest as target build version has yet to be
locked.

## Building

Building requires that you have docker installed on your system which
can be installed using your package manager or you can get more
information here:

https://www.docker.com/get-started

Once docker is installed the container can be built simply by executing
the following command in the root directory of the project:

```shell
docker build -t <TAG_NAME> .
```

Once the container is built you can build the software by following the
command sequence below:

```shell
cd <MBI_SOFTWARE_PROJECT_DIR>
docker run --volume "`pwd`:C:\data" -w "C:\data" --entrypoint "" <TAG_NAME> <BUILD COMMAND>
```
